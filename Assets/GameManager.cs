﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject FX_Sparks;
    ParticleSystem.EmitParams FX_params_sparks;

    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 1.0f;
        FX_params_sparks = new ParticleSystem.EmitParams();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Sparks(Vector2 pos)
    {
        /*
        Debug.Log("This happened");
        FX_params_sparks = new ParticleSystem.EmitParams();
        FX_params_sparks.position = pos;
        FX_Sparks.Emit(FX_params_sparks, 50);
        */
        GameObject sparks = Instantiate(FX_Sparks, pos, Quaternion.identity);
        sparks.GetComponent<ParticleSystem>().Play();
    }


}
