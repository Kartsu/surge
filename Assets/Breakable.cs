﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{

    public float minDashVelocity = 18.0f;

    Explodable explodable;

    GameManager gameManager;

    public AudioSource audioSource;

    public AudioClip breakSound;

    public ScreenShaker screenShaker;
    // Start is called before the first frame update
    void Start()
    {
        explodable = GetComponent<Explodable>();
        gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            Movement movementScript = collision.collider.GetComponent<Movement>();
            if (movementScript.isDashing)
            {
                movementScript.StartEnableGhost();
                Explode(collision);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            Movement movementScript = collision.collider.GetComponent<Movement>();
            if (movementScript.isDashing)
            {
                movementScript.StartEnableGhost();
                Explode(collision);
            }
        }
    }

    void Explode(Collision2D collision)
    {
        screenShaker.ScreenShake();
        explodable.explode();
        audioSource.PlayOneShot(breakSound);
        gameManager.Sparks(collision.transform.position);
    }
}
