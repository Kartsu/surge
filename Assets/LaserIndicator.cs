﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserIndicator : MonoBehaviour
{
    private LineRenderer m_Line;
    public float lineLength = 10f;

    public Vector3 endPos;
    public Vector3 startPos;
    void Start()
    {
        
    }
        


    // Update is called once per frame
    void Update()
    {
        
    }

    void SetCurrentLocation(Vector3 currentLocation)
    {
        startPos = currentLocation;
    }

    void TheStart (Vector3 posToShootAt)
    {
        endPos = posToShootAt;
        Vector2 heading = endPos - startPos;
        float distance = heading.magnitude;
        Vector2 direction = heading.normalized;
        m_Line = GetComponent<LineRenderer>();
        m_Line.SetPosition(0, startPos);
        m_Line.SetPosition(1, new Vector3(startPos.x + direction.x * lineLength, startPos.y + direction.y * lineLength, 0));
    }
}
