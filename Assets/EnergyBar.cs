﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{

    public Slider slider;

    public Gradient gradient;

    public Image fill;

    public Material energyBarMat;

    public void SetEnergy(float energy)
    {
        slider.value = energy;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }

    public void SetMaxHealth(float energy)
    {
        slider.maxValue = energy;
        fill.color = gradient.Evaluate(1f);
    }

    public void EnterOnTheBrink()
    {
        /*
        //float factor = Mathf.Pow(2, intensity);
        Color currentValues = energyBarMat.GetColor("Color_6A30BAF4");
        var intensity = (currentValues.r + currentValues.g + currentValues.b) / 3f;
        var factor = 1f / intensity;
        //Color color = new Color(currentValues.r * factor, currentValues.g * factor, currentValues.b * factor);
        Color color = new Color(currentValues.r * factor, currentValues.g * factor, currentValues.b * factor, currentValues.a);
        energyBarMat.SetColor("Color_6A30BAF4", color);
        energyBarMat.SetVector("_EmissionColor", Color.white * 2.00f);
        */
        Color color = new Color(1.5f, 1.5f, 1.5f);
        energyBarMat.SetColor("Color_6A30BAF4", color);
    }

    public void LeaveOnTheBrink()
    {
        //float intensity = 0.96f;
        //float factor = Mathf.Pow(2, intensity);
        //Color currentValues = energyBarMat.GetColor("Color_6A30BAF4");
        Color color = new Color(1f,1f,1f);
        energyBarMat.SetColor("Color_6A30BAF4", color);
        //energyBarMat.SetVector("_EmissionColor", Color.white * 1.00f);
    }
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
