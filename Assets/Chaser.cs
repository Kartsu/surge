﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chaser : MonoBehaviour
{
    Rigidbody2D rb;
    public GameObject player;
    public GameObject laser;
    public Vector3 startPosition;
    bool isShootingLaser;
    // Start is called before the first frame update
    void Start()
    {
        Physics2D.IgnoreLayerCollision(12, 13);
        Physics2D.IgnoreLayerCollision(12, 14);
        rb = GetComponent<Rigidbody2D>();
        //TurnOnLaser();
        player.GetComponent<Movement>().Respawned += Restart;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Restart()
    {
        transform.position = startPosition;
    }

    IEnumerator ShootLaser()
    {
        for (; ; )
        {
            // execute block of code here
            yield return new WaitForSeconds(3f);
            if (isShootingLaser)
            {
                GameObject go = Instantiate(laser, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                go.SendMessage("SetCurrentLocation", transform.position);
                go.SendMessage("TheStart", player.transform.position);
            }
        }
    }

    public void TurnOnLaser()
    {
        isShootingLaser = true;
        StartCoroutine(ShootLaser());
    }

    public void TurnOffLaser()
    {
        isShootingLaser = false;
        StopCoroutine(ShootLaser());
    }
}
