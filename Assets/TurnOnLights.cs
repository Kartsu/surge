﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Experimental.Rendering.Universal;

public class TurnOnLights : MonoBehaviour
{

    Light2D light;
    public float intensity = 5;
    public float duration = 1;

    // Start is called before the first frame update
    void Awake()
    {
        light = GetComponent<Light2D>();
        DOTween.To(() => light.intensity, x => light.intensity = x, intensity, duration);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
