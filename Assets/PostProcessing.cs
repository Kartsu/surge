﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostProcessing : MonoBehaviour
{
    UnityEngine.Rendering.VolumeProfile volumeProfile;
    UnityEngine.Rendering.Universal.ColorAdjustments colorAdjustments;
    // Start is called before the first frame update
    void Start()
    {
        volumeProfile = GetComponent<UnityEngine.Rendering.Volume>()?.profile;
        if (!volumeProfile) throw new System.NullReferenceException(nameof(UnityEngine.Rendering.VolumeProfile));

        // You can leave this variable out of your function, so you can reuse it throughout your class.

        if (!volumeProfile.TryGet(out colorAdjustments)) throw new System.NullReferenceException(nameof(colorAdjustments));

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetOnTheBrinkPostProcessing()
    {
        colorAdjustments.colorFilter.Override(new Color(255f / 255f, 108f / 255f, 108f / 255f));
        colorAdjustments.saturation.Override(-10f);
    }


    public void SetNormalPostProcessing()
    {
        colorAdjustments.colorFilter.Override(new Color(255f / 255f, 255f / 255f, 255f / 255f));
        colorAdjustments.saturation.Override(0f);
    }
}
