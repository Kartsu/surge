﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleTutorialTrigger : MonoBehaviour
{

    public GameObject text;
    bool firstTime = true;
    bool freezeTime = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(firstTime)
            {
                text.SetActive(true);
                firstTime = false;
                freezeTime = true;
                collision.GetComponent<Movement>().tutorialPause = true;
            }
        }
    }

    private void LateUpdate()
    {
        if(freezeTime)
        {
            freezeTime = false;
            Time.timeScale = 0;
        }
    }
}
