﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnLaserTrigger : MonoBehaviour
{

    public Chaser enemy;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            enemy.TurnOnLaser();
        }
    }
}
