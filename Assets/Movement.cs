﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    public GameObject tutorialText;
    public GameObject obstacleText;
    public GameObject winText;
    public bool tutorialPause = false;
    public bool isDead = false;
    public GameObject chaser;
    public SetParameterByName setParameterByName;
    public SetParameterByName setParameterByName2;
    public Vector3 RespawnPoint;
    public float horizontalMoveSpeed = 40f;
    public float verticalMoveSpeed = 40f;
    public float dashForce = 10f;
    public float energyGainSpeed = 20f;
    public int maxEnergy = 100;
    public float minimumVelocityAfterBreakingObstacle = 5f;
    public ParticleSystem dashParticles;
    public ParticleSystem deathParticle;

    public GameObject exitOutOfBrinkZoneFlash;

    public Transform height;

    public PostProcessing postProcessing;

    public EnergyBar energyBar;

    public GameObject warningCircle;

    public Vector2 previousVelocity = new Vector2(0f,0f);

    private Rigidbody2D rb;

    private BoxCollider2D collider;

    ScreenShaker screenShaker;

    public CameraShake cameraShake;

    public float dashTime = 0.5f;

    float horizontalMove = 0f;
    float verticalMove = 0f;
    bool firstTimeOnTheBrink = true;
    bool dash = false;
    bool onTheBrink = false;
    bool isIncreasedEnergyGain = false;
    public bool isDashing = false;
    private Vector2 dashDirection;
    public float distanceFromChaser = 0f;
    public float onTheBrinkDistance = 20f;
    public float increasedEnergyGainDistance = 25f;
    AudioSource audioSource;
	public FMODUnity.StudioEventEmitter emitter;
    public AudioClip deathSound;
    public GameObject visuals;
    public float respawnTime = 3.0f;
    public GameObject imageRespawn;
    public GameObject imageDie;

    [SerializeField]
    UnityEvent m_Respawn = default;
    public event UnityAction Respawned
    {
        add { m_Respawn.AddListener(value); }
        remove { m_Respawn.RemoveListener(value); }
    }
    void OnRespawn()
    {
        m_Respawn.Invoke();
    }

    // Start is called before the first frame update
    void Start()
    {
        m_Respawn.AddListener(Respawn);
        rb = GetComponent<Rigidbody2D>();
        collider = GetComponent<BoxCollider2D>();
        energyBar.SetMaxHealth(100);
        audioSource = GetComponent<AudioSource>();
        screenShaker = GetComponent<ScreenShaker>();
        transform.position = RespawnPoint;
        Physics2D.IgnoreLayerCollision(8, 14);
		GetComponent<FMODUnity.StudioEventEmitter>().Play();
        Respawn();
    }

    // Update is called once per frame
    void Update()
    {
		distanceFromChaser = Vector3.Distance(transform.position, chaser.transform.position);
        setParameterByName.distance = distanceFromChaser;
        setParameterByName2.distance = distanceFromChaser;
        if (!isDead)
        {
            horizontalMove = Input.GetAxisRaw("Horizontal") * horizontalMoveSpeed;
            verticalMove = Input.GetAxisRaw("Vertical") * verticalMoveSpeed;
        }
        else
        {
            horizontalMove = 0f;
            verticalMove = 0f;
        }
        float energyGain = Mathf.Max((increasedEnergyGainDistance - distanceFromChaser) / energyGainSpeed, 0.01f);
        if(energyGain > 0.01f)
        {
            if(!isIncreasedEnergyGain)
            {
                isIncreasedEnergyGain = true;
                energyBar.EnterOnTheBrink();
            }
        }
        else
        {
            if(isIncreasedEnergyGain)
            {
                isIncreasedEnergyGain = false;
                energyBar.LeaveOnTheBrink();
            }
        }
        energyBar.SetEnergy((energyBar.slider.value + energyGain));
        if (!tutorialPause)
        {
            if (Input.GetButtonDown("Jump"))
            {
                if (energyBar.slider.value - 20 >= 0)
                {
                    energyBar.SetEnergy((int)energyBar.slider.value - 20);
                    dash = true;
                }

            }
            if (distanceFromChaser < onTheBrinkDistance)
            {
                if (!onTheBrink)
                {
                    onTheBrink = true;
                    audioSource.pitch = 1.0f;
                    audioSource.volume = 0.25f;
                    if (firstTimeOnTheBrink)
                    {
                        firstTimeOnTheBrink = false;
                        StartCoroutine(StartTutorialTextAfterDelay());
                    }
                    else
                    {
                        Time.timeScale = 0.5f;
                    }
                    warningCircle.SetActive(true);
                    postProcessing.SetOnTheBrinkPostProcessing();
                }

            }
            else
            {
                if (onTheBrink)
                {
                    onTheBrink = false;
                    audioSource.pitch = 1.0f;
                    audioSource.volume = 1.0f;
                    Time.timeScale = 1.0f;
                    warningCircle.SetActive(false);
                    //exitOutOfBrinkZoneFlash.SetActive(true);
                    postProcessing.SetNormalPostProcessing();
                }
            }
        }
        if((horizontalMove != 0.0f || verticalMove > 0.0f) && Input.GetButtonDown("Jump") && tutorialPause)
        {
            tutorialPause = false;
            if (tutorialText.activeSelf)
            {
                tutorialText.SetActive(false);
            }
            if (obstacleText.activeSelf)
            {
                obstacleText.SetActive(false);
            }
            if(winText.activeSelf)
            {
                winText.SetActive(false);
            }
            dash = true;
            Time.timeScale = 1.0f;
        }
    }

    private void FixedUpdate()
    {

        rb.AddForce(new Vector2(horizontalMove * horizontalMoveSpeed, verticalMove * verticalMoveSpeed));
        if(dash)
        {
            dash = false;
            StartCoroutine(WaitDashCooldown());
            dashDirection = new Vector2(horizontalMove, verticalMove);
            if (distanceFromChaser < onTheBrinkDistance)
            {
                rb.AddForce(new Vector2(dashForce * horizontalMove * 2, dashForce * verticalMove * 2));
            }
            else
            {
                rb.AddForce(new Vector2(dashForce * horizontalMove, dashForce * verticalMove));
            }
            if (horizontalMove != 0) {
                height.GetComponent<Animator>().SetTrigger("squash");
            }
            else if (verticalMove != 0)
            {
                height.GetComponent<Animator>().SetTrigger("stretch");
            }
            //CinemachineShake.Instance.ShakeCamera(5f, 0.1f);
            screenShaker.ScreenShake();
            //StartCoroutine(cameraShake.Shake(0.15f, 0.4f));
            dashParticles.Play();
            previousVelocity = rb.velocity;
			GetComponent<FMODUnity.StudioEventEmitter>().Play();
        }
    }

    IEnumerator WaitDashCooldown()
    {
        isDashing = true;
        yield return new WaitForSeconds(dashTime);
        isDashing = false;
    }

    public void StartEnableGhost()
    {
        /*
        rb.velocity = new Vector2(Mathf.Max(Mathf.Abs(previousVelocity.x), minimumVelocityAfterBreakingObstacle) * Mathf.Sign(previousVelocity.x), 
            Mathf.Max(Mathf.Abs(previousVelocity.y), minimumVelocityAfterBreakingObstacle) * Mathf.Sign(previousVelocity.y));
            */

        print(dashDirection);
        if (distanceFromChaser < onTheBrinkDistance)
        {
            rb.velocity = new Vector2(dashDirection.x * minimumVelocityAfterBreakingObstacle, dashDirection.y * minimumVelocityAfterBreakingObstacle);
            //rb.AddForce(new Vector2(dashForce * dashDirection.x * 2, dashForce * dashDirection.y * 2));
        }
        else
        {
            rb.velocity = new Vector2(dashDirection.x * minimumVelocityAfterBreakingObstacle, dashDirection.y * minimumVelocityAfterBreakingObstacle);
            //rb.AddForce(new Vector2(dashForce * dashDirection.x, dashForce * dashDirection.y));
        }
        /*
        if (distanceFromChaser < onTheBrinkDistance)
        {
            rb.AddForce(new Vector2(dashForce * horizontalMove * 2, dashForce * verticalMove * 2));
        }
        else
        {
            rb.AddForce(new Vector2(dashForce * horizontalMove, dashForce * verticalMove));
        }
        */
    }


    public void Die()
    {
        deathParticle.Play();
        audioSource.PlayOneShot(deathSound);
        rb.isKinematic = true;
        rb.constraints = RigidbodyConstraints2D.FreezeAll;
        collider.isTrigger = true;
        StartCoroutine(Dead());
    }
    IEnumerator Dead()
    {
        isDead = true;
        chaser.GetComponent<Chaser>().TurnOffLaser();
        imageRespawn.SetActive(false);
        imageDie.SetActive(true);
        visuals.GetComponent<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(respawnTime);
        OnRespawn();
    }

    IEnumerator StartTutorialTextAfterDelay()
    {
        yield return new WaitForSeconds(0.25f);
        tutorialText.SetActive(true);
        tutorialPause = true;
        Time.timeScale = 0.0f;
    }

    public void Respawn()
    {
        collider.isTrigger = false;
        chaser.GetComponent<Chaser>().TurnOffLaser();
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        imageDie.SetActive(false);
        imageRespawn.SetActive(true);
        transform.position = RespawnPoint;
        isDead = false;
        visuals.GetComponent<SpriteRenderer>().enabled = true;
    }
}
