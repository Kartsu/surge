﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindController : MonoBehaviour
{

    public Material[] materials;
    public float windSpeed;
    public Material playerTrackMaterial;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (Material material in materials)
        {
            material.SetFloat("_WindSpeed", windSpeed);
        }
        playerTrackMaterial.SetVector("_PlayerPosition", player.transform.position);
        print("Position: " + player.transform.position);
    }
}
