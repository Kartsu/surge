﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterFiveSeconds : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        StartCoroutine(DestroyAfterWaitingFiveSeconds());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DestroyAfterWaitingFiveSeconds()
    {
        yield return new WaitForSeconds(5.0f);
        Destroy(gameObject);
    }
}
