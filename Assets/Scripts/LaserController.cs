﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WGJ145
{
    public class LaserController : MonoBehaviour
    {
        public ParticleSystem laserEndParticles;

        private LineRenderer m_Line;
        private bool endParticlesPlaying;
        private RaycastHit2D hit;
        public float lineLength = 10f;
        public LayerMask PlayerMask;
        private MeshCollider meshCollider;
        [SerializeField]
        private bool isDangerous = false;

        private Vector3 startPos;
        private Vector3 endPos;

        // Start is called before the first frame update
        void Start()
        {
        }

        private void OnEnable()
        {
            m_Line = GetComponent<LineRenderer>();
            meshCollider = GetComponent<MeshCollider>();
            startPos = GetComponentInParent<LaserIndicator>().startPos;
            endPos = GetComponentInParent<LaserIndicator>().endPos;
            // Gets a vector that points from the player's position to the target's.
            Vector2 heading = endPos - startPos;
            float distance = heading.magnitude;
            Vector2 direction = heading.normalized;
            m_Line.SetPosition(0, startPos);
            m_Line.SetPosition(1, new Vector3(startPos.x + direction.x * lineLength, startPos.y + direction.y * lineLength, 0));
            if (isDangerous)
            {
                hit = Physics2D.Raycast(startPos, direction, lineLength, PlayerMask);
                if(hit)
                {
                    if (!hit.collider.GetComponent<Movement>().isDead)
                    {
                        hit.collider.GetComponent<Movement>().Die();
                    }
                }
                //AddColliderToLine(m_Line, startPos, new Vector3(startPos.x + direction.x * lineLength, startPos.y + direction.y * lineLength, 0));
            }
        }

        // Update is called once per frame
        void Update()
        {
            /*
            if (m_Line.enabled)
            {
                //hit = Physics2D.Raycast(transform.position, transform.right, lineLength, GroundLayerMask);
                hit = Physics2D.Linecast(startPos, endPos, GroundLayerMask);
                if (hit)
                {
                    if (endParticlesPlaying == false)
                    {
                        endParticlesPlaying = true;
                        laserEndParticles.Play(true);
                    }
                    float distance = ((Vector2)hit.point - (Vector2)transform.position).magnitude;
                    m_Line.SetPosition(1, new Vector3(distance, 0, 0));
                    if (isDangerous)
                    {
                        AddColliderToLine(m_Line, transform.position, hit.point);
                    }
                    laserEndParticles.gameObject.transform.position = hit.point;

                }
                else
                {
                    m_Line.SetPosition(1, new Vector3(lineLength, 0, 0));
                    endParticlesPlaying = false;
                    laserEndParticles.Stop(true);
                }
            }
            else
            {
                m_Line.SetPosition(1, new Vector3(lineLength, 0, 0));
                endParticlesPlaying = false;
                laserEndParticles.Stop(true);
            }
            */
        }

        void FireLaser()
        {
            m_Line.enabled = true;
        }

        void EndLaser()
        {
            m_Line.enabled = false;
        }

        private void AddColliderToLine(LineRenderer line, Vector3 startPoint, Vector3 endPoint)
        {
            //create the collider for the line
            BoxCollider lineCollider = new GameObject("LineCollider").AddComponent<BoxCollider>();
            //set the collider as a child of your line
            lineCollider.transform.parent = line.transform;
            // get width of collider from line 
            float lineWidth = line.endWidth;
            // get the length of the line using the Distance method
            float lineLength = Vector3.Distance(startPoint, endPoint);
            // size of collider is set where X is length of line, Y is width of line
            //z will be how far the collider reaches to the sky
            lineCollider.size = new Vector3(lineLength, lineWidth, 1f);
            // get the midPoint
            Vector3 midPoint = (startPoint + endPoint) / 2;
            // move the created collider to the midPoint
            lineCollider.transform.position = midPoint;


            //heres the beef of the function, Mathf.Atan2 wants the slope, be careful however because it wants it in a weird form
            //it will divide for you so just plug in your (y2-y1),(x2,x1)
            float angle = Mathf.Atan2((endPoint.z - startPoint.z), (endPoint.x - startPoint.x));

            // angle now holds our answer but it's in radians, we want degrees
            // Mathf.Rad2Deg is just a constant equal to 57.2958 that we multiply by to change radians to degrees
            angle *= Mathf.Rad2Deg;

            //were interested in the inverse so multiply by -1
            angle *= -1;
            // now apply the rotation to the collider's transform, carful where you put the angle variable
            // in 3d space you don't wan't to rotate on your y axis
            lineCollider.transform.Rotate(0, angle, 0);
        }
    }
}