﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ScreenShaker : MonoBehaviour
{
    CinemachineImpulseSource m_CameraImpulse = default;
    //AudioSource m_audioSource = default;

    // Start is called before the first frame update
    void Start()
    {
        m_CameraImpulse = GetComponent<CinemachineImpulseSource>();
        //m_audioSource = GetComponent<AudioSource>();
    }


    public void ScreenShake()
    {
        m_CameraImpulse.GenerateImpulse();
    }
    /*
    public void PlayAudioSource()
    {
        m_audioSource.Play();
    }
    */
}
