﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTrigger : MonoBehaviour
{

    public GameObject gameObjectToShow;
    public AudioClip[] triggerSounds;
    public AudioSource audioSource;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            if (!gameObjectToShow.activeSelf) {
                if (triggerSounds != null)
                {
                    if (triggerSounds.Length != 0)
                    {
                        int index = Random.Range(0, triggerSounds.Length - 1);
                        audioSource.PlayOneShot(triggerSounds[index]);
                    }
                }
                gameObjectToShow.SetActive(true);
            }
        }
    }
}
