﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Room
{
    void OnTriggerEnter2D(Collider2D other);

    void OnTriggerExit2D(Collider2D other);

    Transform getSpawnPoint();

    GameObject getVirtualCam();
}
